svg4everybody();

(function($) {

  const breakpoints = {
    small: 768,
    medium: 992,
    large: 1200
  };

  let $siteHeader;
  let $primaryNav;
  let $secondaryNav;
  let $tertiaryNavMenu;

  function moveTertiaryNav() {
    if (window.innerWidth >= breakpoints.small) {
      $tertiaryNavMenu.appendTo($secondaryNav);
    } else {
      $tertiaryNavMenu.appendTo($primaryNav);
    }
  }

  function fixHeader() {
    if ($(window).scrollTop() > 230) {
      $siteHeader.addClass('is-fixed');
    } else {
      $siteHeader.removeClass('is-fixed');
    }
  }

  $(function() {
    $siteHeader = $('.site-header');
    $primaryNav = $('#primary-nav');
    $secondaryNav = $('#secondary-nav');
    $tertiaryNavMenu = $('#tertiary-nav-menu');

    $('[data-open-close]').on('click', function(e) {
      e.preventDefault();
      var toggle = $(this).attr('data-open-close');
      $('#' + toggle).toggleClass('is-open');
    });

    $(window).on('resize', moveTertiaryNav);
    moveTertiaryNav();

    $(window).on('scroll', fixHeader);
    fixHeader();
  });

})(jQuery);
